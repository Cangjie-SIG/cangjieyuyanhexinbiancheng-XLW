AutoPrintable
=============

The project implements a trivial macro to implement the `ToString` interface
automatically for records and classes, e.g. by adding the `@AutoPrintable`
decorator.

For example,

    import macro auto_printable.*

    @AutoPrintable
    record SomeThing {
        var a: Int32
        var b: Int32
        ...
    }

and then we can print any instance of `SomeThing`


Testing
-------

For now I an not fully familiar with the `cpm` package manager.

So please just run `make test` and run the example program by `./test`, ;)

Limitation
----------

The current implementation does not support the named constructor, e.g.

```
@AutoPrintable
record A {
    A(let a: Int32)
}
```
