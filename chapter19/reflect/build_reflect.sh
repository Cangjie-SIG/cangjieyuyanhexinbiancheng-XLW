#! /bin/bash

MODULE_NAME=reflect

src=`pwd`/${MODULE_NAME}/src
REFLECT_TYPE=${src}/typ
REFLECT_CORE=${src}/core


REFLECT_TYPE_LIB_NAME=${MODULE_NAME}_typ
REFLECT_CORE_LIB_NAME=${MODULE_NAME}_core


BUILD_DIST_PATH=build/${MODULE_NAME}

export CANGJIE_PATH=build/:$CANGJIE_PATH

if [ -d "build" ]; then 
    rm -rf build
fi

mkdir -p ${BUILD_DIST_PATH}

cjc -p ${REFLECT_TYPE} --module-name ${MODULE_NAME} --output-type=staticlib -o ${BUILD_DIST_PATH}/lib${REFLECT_TYPE_LIB_NAME}.a -V

cjc -p ${REFLECT_CORE} --module-name ${MODULE_NAME} --output-type=dylib -L ${BUILD_DIST_PATH} -l${REFLECT_TYPE_LIB_NAME} -o ${BUILD_DIST_PATH}/lib${REFLECT_CORE_LIB_NAME}.so -V
