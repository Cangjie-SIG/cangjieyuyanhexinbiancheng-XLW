# reflect

> 仓颉语言反射库，用于编译期代码注入

## 路线
![./assets/路线.png](./assets/路线.png)


## 构建

### 构建核心代码

`./build_reflect.sh`

### 构建测试代码

`./build_test.sh`

## 仓库结构

```
|- reflect          ------------ 核心代码
  |- src
    |- core         ------------ 反射库核心处理逻辑代码
    |- typ          ------------ 反射库类型代码
|- test             ------------ 测试代码
  |- src
|- build_reflect.sh ------------ 核心代码构建脚本
|- build_test.sh    ------------ 测试代码构建脚本
```

## 使用

### 添加宏

** 注意：如需使用setter，请在属性声明时添加显式属性，并用`var`声明。否则在使用对应属性`setValue`时会抛异常。 **

```
from std import ast.*
from reflect import core.*
from reflect import typ.*

@Reflect
class A<T> {
    private var vv: T
    public let mm = "ad"

    init(v: T, mm: String) {
        this(v)
    }

    public init(v: T) {
        this.vv = v
    }

    func `main`() {
        var dd = 1
    }

    public func a() {}
    var cc: collection.ArrayList<Int64> = collection.ArrayList<Int64>()
    var dd: Int64 = 0
}
```

### 静态方法

#### 获取所有成员属性类型

```cangjie
main() {
    let types = A<String>.getReflectFields()
    for (t in types) {
        println(t)
    }
}
```

#### 按属性名获取属性类型

```cangjie
main() {
    let mmType = A<String>.getReflectField("mm")
    match (mmType) {
        case Err(e) => println(e)
        case Ok(r) => println(r)
    }
}
```

#### 获取所有成员属性名列表

```cangjie
main() {
    for (k in A<String>.getReflectFieldNames()) {
        println(k)
    }
}
```

### 引用方法

#### 获取所有成员属性值

```cangjie
main() {
    let a = A<String>("Cangjie")
    
    let values = a.getReflectFieldValues()
    for (value in values) {
        let v = value.getValue()
        match (v as String) {
            case None => println("${value}")
            case Some(r) => println("${value.getName()} = ${r}")
        }
    }
}
```

#### 按属性名获取属性值

```cangjie
main() {
    let a = A<String>("Cangjie")
    
    let vvValue = a.getReflectFieldValue("vv")
    match (vvValue) {
        case Err(e) => println(e)
        case Ok(r) => 
            match (r.getValue() as String) {
                case None => println("无法转换")
                case Some(r0) => println(r0)
            }
    }
}
```

#### 设置属性值

```cangjie
main() {
    let a = A<String>("Cangjie")
    
    let vvValue = a.getReflectFieldValue("vv")
    match (vvValue) {
        case Err(e) => println(e)
        case Ok(r) => 
            r.setValue("Changeden")
            match (r.getValue() as String) {
                case None => println("无法转换")
                case Some(r0) => println(r0)
            }
    }
}
```

#### 获取所有成员属性名列表

```cangjie
main() {
    let a = A<String>("Cangjie")
    
    for (k in a.getReflectFieldValueNames()) {
        println(k)
    }
}
```
