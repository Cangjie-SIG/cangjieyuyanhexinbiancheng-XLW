// drawlib.c
/**
 Linux (POSIX) implementation of _kbhit().
 Morgan McGuire, morgan@cs.brown.edu
 */
#include <stdio.h>
#include <sys/select.h>
#include <termios.h>
#include <stropts.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <stdlib.h>

enum
{
	FG_BLACK      = 30,
	FG_RED        = 31,
	FG_GREEN      = 32,
	FG_YELLOW     = 33,
	FG_BLUE       = 34,
	FG_MAGENTA    = 35,
	FG_CYAN       = 36,
	FG_WHITE      = 37
};

enum
{
	BG_BLACK      = 40,
	BG_RED        = 41,
	BG_GREEN      = 42,
	BG_YELLOW     = 43,
	BG_BLUE       = 44,
	BG_MAGENTA    = 45,
	BG_CYAN       = 46,
	BG_WHITE      = 47
};

enum
{
	TS_NORMAL     = 0,
	TS_BOLD       = 1,
	TS_UNDERSCORE = 4,
	TS_BLINK      = 5,
	TS_REVERSE    = 7
};

void terminal_clear(void);
void terminal_set_color_bg(int bg_color);
void terminal_set_color_fg(int fg_color);
void terminal_set_cursor_xy(int x, int y);
void terminal_set_text_style(int attr);
void terminal_get_size(int *w, int *h);
int kbhit(void);
char getch(int echo);

void terminal_clear(void)
{
	fputs("\x1B[2]", stdout);
}

void terminal_set_color_bg(int bg_color)
{
	printf("\x1B[%dm", bg_color);
}

void terminal_set_color_fg(int fg_color)
{
	printf("\x1B[%dm", fg_color);
}

void terminal_set_cursor_xy(int x, int y)
{
	printf("\x1B[%d;%dH", x, y);
}

void terminal_set_text_style(int attr)
{
	printf("\x1B[%dm", attr);
}

void console_get_size(int *w, int *h)
{
	struct winsize wsize;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &wsize);
	*w = wsize.ws_row;
	*h = wsize.ws_col;
}

int kbhit(void)
{
	int byteswaiting;
	static struct termios term, term2;
	tcgetattr(0, &term);
	term2 = term;
	term2.c_lflag &= ~ICANON;
	tcsetattr(0, TCSANOW, &term2);
	ioctl(0, FIONREAD, &byteswaiting);
	tcsetattr(0, TCSANOW, &term);
	return byteswaiting > 0;
}

char getch(int echo)
{
	static struct termios kbd_old, kbd_new;
	char ch;
	tcgetattr(0, &kbd_old);
	kbd_new = kbd_old;
	kbd_new.c_lflag &= ~ICANON;
	if(echo)
	{
		kbd_new.c_lflag |= ECHO;
	}
	else
	{
		kbd_new.c_lflag &= ~ECHO;
	}

	tcsetattr(0, TCSANOW, &kbd_new);
	ch = getchar();
	tcsetattr(0, TCSANOW, &kbd_old);
	return ch;
}

int _kbhit() {
    static const int STDIN = 0;
    static bool initialized = false;

    if (! initialized) {
        // Use termios to turn off line buffering
        struct termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}

bool is_kbhit() {
    if (! _kbhit()) {
        return false;
    }
    else {
        return true;
    }
}

char is_getch() {
    return getch(0); // echo 是什么？
}


// 清屏
void clrScreen() {
	static bool isclr = true;
	if (isclr) {
		// warning: ignoring return value of ‘system’, declared with attribute warn_unused_result
		int v = system("clear");

		// 清除屏幕
		//printf("\033[2J");

		// 隐藏光标
		printf("\033[?25l");
	}
	else {
		// 光标复位
		printf("\033[H");
		isclr = false;
	}
}

void sleepScreen(long param) {
    long real = param * 1000;
    // sleep(real); // 1秒
    usleep(real);
}

// 以上两个函数仅限Linux环境