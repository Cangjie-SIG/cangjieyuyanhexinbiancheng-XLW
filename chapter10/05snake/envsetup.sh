#!/bin/bash
# Program:
# 	Try do add LD_LIBRARY_PATH to ~/.bashrc.
# History:
# 2022/05/12	Jiamiao532	First release
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

# Get current shell name.
shell_path=$(readlink -f /proc/$$/exe)
shell_name=${shell_path##*/}

# Get the absolute path of this script accroding to different shells.
case ${shell_name} in
    "zsh")
        script_dir=$(cd "$(dirname "${(%):-%N}")" || exit; pwd) # ShellCheck does not support Zsh.
        ;;
    "sh" | "bash")
        script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" || exit; pwd)
        ;;
    *)
        echo "[ERROR] Unsupported shell: ${shell_name}, please switch to bash, sh or zsh."
        return 1
        ;;
esac

# 仅对本次login生效
# export LD_LIBRARY_PATH=${script_dir}/src/thirdParty/solib:$LD_LIBRARY_PATH

new_env="${script_dir}/src/thirdParty/solib"
result=$(env | grep "${new_env}")
if [ "${result}" != "" ]; then
    printf "The path already exists in LD_LIBRARY_PATH\n"
    exit
fi

# 需要改进
# 向~/.bashrc追加写
printf "\nexport LD_LIBRARY_PATH=%s/src/thirdParty/solib:\$LD_LIBRARY_PATH" "${script_dir}" >> ~/.bashrc

# hint
printf "envsetup success\n"

# test subprocess
# result shell script can not include source
# printf "\nexport A=1" >> ~/.bashrc
# source ~/.bashrc
