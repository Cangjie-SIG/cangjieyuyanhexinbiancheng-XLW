## 项目介绍

用仓颉语言开发一个Linux控制台贪吃蛇游戏。

## 编译方法

在解压后目录下，首先运行

```make all```

编译所有的C依赖。

其次，运行

```bash ./envsetup.sh```

在~/.bashrc文件中添加LD_LIBRARY_PATH

运行

```source ~/.bashrc```

更新路径。

最后编译整个模块。

```cpm build```

如果编译成功，生成的可执行文件在

```./bin/main```