#include <stdio.h>
#include <sys/select.h>
#include <termios.h>
#include <stropts.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <stdlib.h>

int kbhit(void)
{
	int byteswaiting;
	static struct termios term, term2;
	tcgetattr(0, &term);
	term2 = term;
	term2.c_lflag &= ~ICANON;
	tcsetattr(0, TCSANOW, &term2);
	ioctl(0, FIONREAD, &byteswaiting);
	tcsetattr(0, TCSANOW, &term);
	return byteswaiting > 0;
}

//获取按键名称
char getch(int echo)
{
	static struct termios kbd_old, kbd_new;
	char ch;
	tcgetattr(0, &kbd_old);
	kbd_new = kbd_old;
	kbd_new.c_lflag &= ~ICANON;
	if (echo)
	{
		kbd_new.c_lflag |= ECHO;
	}
	else
	{
		kbd_new.c_lflag &= ~ECHO;
	}

	tcsetattr(0, TCSANOW, &kbd_new);
	ch = getchar();
	tcsetattr(0, TCSANOW, &kbd_old);
	return ch;
}

int _kbhit()
{
	static const int STDIN = 0;
	static bool initialized = false;

	if (!initialized)
	{
		// 使用termios关闭线路缓冲
		struct termios term;
		tcgetattr(STDIN, &term);
		term.c_lflag &= ~ICANON;
		tcsetattr(STDIN, TCSANOW, &term);
		setbuf(stdin, NULL);
		initialized = true;
	}
	int bytesWaiting;
	ioctl(STDIN, FIONREAD, &bytesWaiting);
	return bytesWaiting;
}

//判断按键输入
bool is_kbhit()
{
	if (!_kbhit())
	{
		return false;
	}
	else
	{
		return true;
	}
}

//是否可以获取按键的名称
char is_getch()
{
	return getch(0);
}


//清除终端
void clrScreen()
{
	static bool isclr = true;
	if (isclr)
	{
		int v = system("clear");
		// 隐藏光标
		printf("\033[?25l");
	}
	else
	{
		// 光标复位
		printf("\033[H");
		isclr = false;
	}
}

//设置sleep的时间
void sleepScreen(long param)
{
	long real = param * 1000;
	// sleep(real); // 1秒
	usleep(real);
}
