from ctypes import *


def foo(func):
    func(True, 10, 40)

# func是传入的仓颉函数
def fooptr(func):
    a = c_int(10)
    #调用仓颉函数
    func(pointer(a), c_char_p(b'abc'), c_wchar_p('def'))
