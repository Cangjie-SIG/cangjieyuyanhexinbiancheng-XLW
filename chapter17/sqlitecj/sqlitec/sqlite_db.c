#include <stdio.h>
#include "sqlite3.h"

sqlite3 *db;

void cj_sqlite_open(char *path)
{
    sqlite3_open(path, &db);
    printf("%s\n",path);
    printf("%s\n","sqlite 连接成功");
}

void cj_sqlite_exec(char *sql)
{
    sqlite3_exec(db, sql, 0, 0, 0);
    sql = NULL;
    printf("%s\n","sql 执行成功");
}

void cj_sqlite_close()
{
    sqlite3_close(db);
    printf("%s\n","db关闭");
}
