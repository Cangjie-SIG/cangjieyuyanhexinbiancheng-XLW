# protobuf-cj 测试用例说明
## 一，pb-plugin编译
执行本仓的顶级目录下`build.sh`，会生成`protoc-gen-cj`插件。
```
 sh build.sh
```

## 二，protoc工具安装
在`ubunut18.04`编译环境中，安装protoc工具，例如版本如下。
```
~$ uname -a
Linux cangjie 5.4.0-92-generic #103~18.04.2-Ubuntu SMP Wed Dec 1 16:50:36 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux

~$ protoc --version
libprotoc 3.5.1
```

## 三，测试用例编译

1，使用`protoc-gen-cj`插件和example.proto 生成仓颉语法文件。例如进入路径`test/LLT/testcase0001`下
```
protoc --plugin=../../../protoc-gen-cj --cj_out=. example.proto
```
执行该条命令后， 会新增`foo`路径下的`example.pb.cj`文件。

2，在`testcase0001`下，编译生成`libprotobuf.a`库文件，及`protobuf.cjo`:
```
cjc -p ../../../src/protobuf/ --output-type=staticlib

ar r libprotobuf.a ../../../ffi.o
```

3，在`testcase0001`下，编译生成`libfoo.a`库文件，及`foo.cjo`文件。
```
cjc -p foo/ --output-type=staticlib
```

4，编译测试用例`serialize.cj`，会把序列化结果保存到文件`pb_cj2java.bin`中。
```
cjc serialize.cj -L . -l foo -l protobuf -o serialize
```
## 四，打印日志
1， 执行 `./serialize`，打印输出如下所示。
```
// 参数赋值
{"name": "test", "id": 1, "email": "test@huawei.com"}
// 序列化后的数组
[10, 4, 116, 101, 115, 116, 16, 1, 26, 15, 116, 101, 115, 116, 64, 104, 117, 97, 119, 101, 105, 46, 99, 111, 109]
```
2，`pb_cj2java.bin`中16进制数据内容如下所示。
```
// hexdump: 十六进制数据查看工具
hexdump -C pb_cj2java.bin
```
```
00000000  0a 04 74 65 73 74 10 01  1a 0f 74 65 73 74 40 68  |..test....test@h|
00000010  75 61 77 65 69 2e 63 6f  6d                       |uawei.com|
00000019
```

## 五，验证数据

1，仓颉端到java端的文件解码

序列化后的文件`pb_cj2java.bin`， 传入到`java protobuf`端解析，如果解析成功，和`cangjie`端的参数赋值是相同的，表明该用例涉及的.proto文件解析，及编码(pack)接口功能是正常的。

2，java端到仓颉端的文件解码
序列化后的文件`pb_java2cj.bin`， 传入到`cangjie protobuf`端解析，如果解析结果和java端的参数赋值是相同的，表明该用例涉及的解码(unpack)接口功能是正常的。

