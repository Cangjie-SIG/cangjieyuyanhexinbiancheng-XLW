#! /bin/bash

if [ "$1" = "clean" ]; then
    rm *.cjo *.a *.bin *.json serialize -f
    rm foo/ -rf

else
protoc --plugin=../../../protoc-gen-cj --cj_out=. example.proto

cjc -p ../../../src/protobuf/ --output-type=staticlib
ar r libprotobuf.a ../../../ffi.o

cjc -p foo/ --output-type=staticlib

cjc serialize.cj -L . -l foo -l protobuf -o serialize
./serialize

cjc deserialize.cj -L . -l foo -l protobuf -o deserialize
./deserialize

fi

exit 0
