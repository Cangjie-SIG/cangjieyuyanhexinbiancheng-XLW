import java.io.IOException;

import foo.Example;
public class ProtobufEncode {
        public static void main(String[] args) throws IOException {

            Example.Person.Builder personBuilder = Example.Person.newBuilder();

            personBuilder.setId(1);
            personBuilder.setName("test");
            personBuilder.setEmail("test@huawei.com");

            Example.Person.PhoneNumber.Builder PhoneNumberBuilder = Example.Person.PhoneNumber.newBuilder();
            PhoneNumberBuilder.setNumber("12345678901");
            PhoneNumberBuilder.setType(Example.Person.PhoneType.WORK);

            Example.Person.PhoneNumber.Comment.Builder CommentBuilder = Example.Person.PhoneNumber.Comment.newBuilder();
            CommentBuilder.setComment1("colleague");

            System.out.println(personBuilder);
            System.out.println(PhoneNumberBuilder);
            System.out.println(CommentBuilder);

            personBuilder.addPhone(PhoneNumberBuilder.setComment(CommentBuilder));

            Example.Person person = personBuilder.build();
            System.out.println(person.getSerializedSize());

            // java端protobuf 序列化数据保存到pb_java2cj.bin文件中。
            byte[] buffToWrite = person.toByteArray();
            System.out.println(buffToWrite.toString());
            FileReadWrite readWrite = new FileReadWrite();
            readWrite.write(buffToWrite);
    }
}
