#! /bin/bash

protoc --plugin=../../../protoc-gen-cj --cj_out=. example.proto

cjc -p ../../../src/protobuf/ --output-type=staticlib
ar r libprotobuf.a ../../../ffi.o

cjc -p foo/ --output-type=staticlib

cjc serialize.cj -L . -l foo -l protobuf -o serialize
./serialize

exit 0
