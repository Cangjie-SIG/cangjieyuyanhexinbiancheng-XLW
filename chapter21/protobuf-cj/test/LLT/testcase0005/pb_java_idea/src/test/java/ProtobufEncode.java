import java.io.IOException;

import foo.Example;
public class ProtobufEncode {
        public static void main(String[] args) throws IOException {

            Example.Title.Builder titleBuilder = Example.Title.newBuilder();

            Example.Title1.Builder title1Builder = Example.Title1.newBuilder();
            titleBuilder.setTitle1(title1Builder.setName("title1").setVersion("v1.0"));

            Example.Title2.Builder title2Builder = Example.Title2.newBuilder();
            titleBuilder.setTitle2(title2Builder.setAuthor("zhangsan").setCategory("science"));

            Example.Title3.Builder title3Builder = Example.Title3.newBuilder();
            titleBuilder.setTitle3(title3Builder.setPages(125).setDate("2022.05.31")
                    .setProperty("prose"));

            System.out.println(titleBuilder);

            Example.Title title = titleBuilder.build();
            System.out.println(title.getSerializedSize());

            // java端protobuf 序列化数据保存到pb_java2cj.bin文件中。
            byte[] buffToWrite = title.toByteArray();
            System.out.println(buffToWrite.toString());
            FileReadWrite readWrite = new FileReadWrite();
            readWrite.write(buffToWrite);
    }
}
