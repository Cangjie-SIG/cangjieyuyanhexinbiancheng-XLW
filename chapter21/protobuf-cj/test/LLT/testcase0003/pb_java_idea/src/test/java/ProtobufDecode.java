import java.io.IOException;
import java.util.List;

import foo.Example;

public class ProtobufDecode {
        public static void main(String[] args) throws IOException {
            FileReadWrite readWrite = new FileReadWrite();
            byte[] buffToRead = readWrite.read();

            // 从cj_pb.txt中读取数据，在java端进行反序列化。
            Example.Person personOut = Example.Person.parseFrom(buffToRead);
            List<Example.Person.PhoneNumber> phoneList = personOut.getPhoneList();
            System.out.println(personOut.getPhoneList());
            System.out.println(personOut.toString());
    }
}
