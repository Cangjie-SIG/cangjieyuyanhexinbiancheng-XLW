import java.io.IOException;
import foo.Example;

public class ProtobufEncode {
        public static void main(String[] args) throws IOException {

            Example.Person.Builder personBuilder = Example.Person.newBuilder();

            personBuilder.setId(1);
            personBuilder.setName("test");
            personBuilder.setEmail("test@huawei.com");

            System.out.println(personBuilder);

            Example.Person person = personBuilder.build();

            // java端protobuf 序列化数据保存到pb_java2cj.txt文件中。
            byte[] buffToWrite = person.toByteArray();
            System.out.println(buffToWrite.toString());
            FileReadWrite readWrite = new FileReadWrite();
            readWrite.write(buffToWrite);
    }
}
