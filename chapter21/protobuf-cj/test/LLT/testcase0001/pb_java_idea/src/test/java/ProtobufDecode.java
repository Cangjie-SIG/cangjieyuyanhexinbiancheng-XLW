import java.io.IOException;
import foo.Example;

public class ProtobufDecode {
        public static void main(String[] args) throws IOException {
//            Example.Person.Builder personBuilder = Example.Person.newBuilder();
            FileReadWrite readWrite = new FileReadWrite();
            byte[] buffToRead = readWrite.read();
            System.out.println(buffToRead);

            // 从cj_pb.txt中读取数据，在java端进行反序列化。
            Example.Person personOut = Example.Person.parseFrom(buffToRead);
            System.out.println(personOut.toString());
    }
}
