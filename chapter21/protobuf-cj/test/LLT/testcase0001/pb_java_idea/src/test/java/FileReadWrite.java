import java.io.*;

public class FileReadWrite {

    public byte[] read() throws IOException {
        File file = new File("./pb_cj2java.bin");
        FileInputStream inputStream = new FileInputStream(file);
        byte[] bytes = inputStream.readAllBytes();
        inputStream.close();
        return bytes;
    }

    public void write(byte[] buffer) throws IOException {
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream("pb_java2cj.txt"));
        stream.write(buffer);
        stream.flush();
        stream.close();
    }
}
