import java.io.IOException;

import com.google.common.primitives.Bytes;
import com.google.protobuf.ByteString;
import foo.Example;

public class ProtobufDecode {
        public static void main(String[] args) throws IOException {
            FileReadWrite readWrite = new FileReadWrite();
            byte[] buffToRead = readWrite.read();
//            System.out.println(buffToRead);

            // 从cj_pb.txt中读取数据，在java端进行反序列化。
            Example.ValueTypes valueTypes = Example.ValueTypes.parseFrom(buffToRead);

//            System.out.println("d7######:" + Integer.toHexString(valueTypes.getD7()));
//            System.out.println(Long.toHexString(valueTypes.getD8()));
//            System.out.println(valueTypes.getD9() + ", " + Long.toHexString(valueTypes.getD9()));
//            System.out.println(Long.toHexString(valueTypes.getD10()));
//            ByteString b1 = valueTypes.getB1();
//            System.out.println(b1.toString());
//              System.out.println(Integer.toString(valueTypes.getD1()));
            System.out.println("cangjie to java, deserialize result:");
            System.out.println(valueTypes.toString());
        }
}
