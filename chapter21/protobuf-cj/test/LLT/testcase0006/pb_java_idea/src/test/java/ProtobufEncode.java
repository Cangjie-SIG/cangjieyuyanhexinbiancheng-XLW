import java.io.IOException;

import com.google.protobuf.ByteString;
import com.google.protobuf.UInt32Value;
import foo.Example;

public class ProtobufEncode {
        public static void main(String[] args) throws IOException {

           Example.ValueTypes.Builder valueTypeBuilder = Example.ValueTypes.newBuilder();

           valueTypeBuilder.setD1(2147483647);
           valueTypeBuilder.setD2(-2147483646);
           valueTypeBuilder.setD3(214748362);
           valueTypeBuilder.setD4(-214748);
           valueTypeBuilder.setD5(-2147483643);
           valueTypeBuilder.setD6(-2147483642);
           valueTypeBuilder.setD7(2147483642);
           valueTypeBuilder.setD8(-214748364);
           valueTypeBuilder.setD9(-21474836);
           valueTypeBuilder.setD10((long) 214748361);
           valueTypeBuilder.setD11(3.141592535);
           valueTypeBuilder.setF1((float) 3.1415926);
           valueTypeBuilder.setFlag(true);
           byte[] data = {3, 1, 4, 1, 5, 9, 2, 6};
           valueTypeBuilder.setB1(ByteString.copyFrom(data));
           valueTypeBuilder.setS1("value types test");
           System.out.println("java, serialize result:");
           System.out.println(valueTypeBuilder);

           Example.ValueTypes valueTypes = valueTypeBuilder.build();

           // java端protobuf 序列化数据保存到pb_java2cj.bin文件中。
           byte[] buffToWrite = valueTypes.toByteArray();
           System.out.println(buffToWrite.toString());
           FileReadWrite readWrite = new FileReadWrite();
           readWrite.write(buffToWrite);
    }
}
