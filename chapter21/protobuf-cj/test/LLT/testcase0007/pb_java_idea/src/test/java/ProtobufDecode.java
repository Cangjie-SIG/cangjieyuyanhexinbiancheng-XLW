import java.io.IOException;
import java.util.Map;

import foo.Example;

public class ProtobufDecode {
        public static void main(String[] args) throws IOException {
            FileReadWrite readWrite = new FileReadWrite();
            byte[] buffToRead = readWrite.read();
            System.out.println(buffToRead);

            // 从cj_pb.txt中读取数据，在java端进行反序列化。
            Example.Person person = Example.Person.parseFrom(buffToRead);
            System.out.println("name: " + person.getName());
            System.out.println("phone: " + person.getPhone());
            System.out.println("email: " + person.getEmail());
            System.out.println("age: " + person.getAge());
            Map<String, Integer> familyListMap = person.getFamilyListMap();
            for (Map.Entry<String, Integer> entry: familyListMap.entrySet()) {
                System.out.println(entry.getKey() + ", " + entry.getValue());
            }
//            System.out.println(person.toString());
    }
}
