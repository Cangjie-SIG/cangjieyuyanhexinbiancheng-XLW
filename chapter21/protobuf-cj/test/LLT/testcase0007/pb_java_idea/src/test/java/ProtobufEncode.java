import java.io.IOException;

import foo.Example;
public class ProtobufEncode {
        public static void main(String[] args) throws IOException {

            Example.Person.Builder personBuilder = Example.Person.newBuilder();
            personBuilder.setName("zhangsan");
            personBuilder.setPhone("1234567890");
            personBuilder.setEmail("zhangsan.qq.com");
            personBuilder.setAge(26);
            personBuilder.putFamilyList("father", 51).putFamilyList("mother", 50);
            Example.Person person = personBuilder.build();
            System.out.println(person.getSerializedSize());

            // java端protobuf 序列化数据保存到pb_java2cj.bin文件中。
            byte[] buffToWrite = person.toByteArray();
            System.out.println(buffToWrite.toString());
            FileReadWrite readWrite = new FileReadWrite();
            readWrite.write(buffToWrite);
    }
}
