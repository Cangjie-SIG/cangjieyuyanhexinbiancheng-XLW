#! /bin/bash

# ffi.o
gcc -g -c ./c/ffi.c -o ./ffi.o

src=`pwd`/src
PROTOC=${src}/protoc
PROTOBUF=${src}/protobuf
GOOGLE_PROTOBUF=${src}/google/protobuf
GOOGLE_PROTOC=${src}/google/protobuf/compiler
export CANGJIE_PATH=${GOOGLE_PROTOBUF}:${GOOGLE_PROTOC}:${PROTOC}:${PROTOBUF}

# protoc-gen-cj (源码方式)
cjc -g -V -p src/ -p src/protoc/ -p src/google/protobuf/ -p src/google/protobuf/compiler/ -p src/protobuf/ ./ffi.o -o ./protoc-gen-cj

# protoc-gen-cj (链接protobuf库方式)
# # libprotobuf.a
# cjc -g -V --output-type=staticlib -p src/protobuf/ --output-dir src/protobuf
# # insert ffi.o to libprotobuf.a
# ar r src/protobuf/libprotobuf.a ffi.o
# cjc -g -V -p src/ -p src/protoc/ -p src/google/protobuf/ -p src/google/protobuf/compiler/ --import-path src/protobuf -L src/protobuf -l protobuf -o ./protoc-gen-cj

# protoc
protoc --plugin=./protoc-gen-cj --cj_out=./src \
    -Isrc \
    src/google/protobuf/descriptor.proto \
    src/google/protobuf/compiler/plugin.proto \
    src/protobuf/protobuf.proto