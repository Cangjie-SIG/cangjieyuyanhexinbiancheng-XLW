### 接口说明

主要是核心类和成员函数说明

#### `class Int32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class UInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class Int64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class UInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class EnumAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func toString(val: Any)
public static func getInstance()
```

#### `class SInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class SInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class BoolAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class SFixed32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class Fixed32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class FloatAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class SFixed64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class Fixed64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class DoubleAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class StringAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func toString(val: Any)
public static func getInstance()
```

#### `class BytesAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class MessageAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func clone(val: Any)
public static func getInstance()
```

#### `class RepeatInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatUInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedUInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatUInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedUInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatEnumAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func toString(val: Any)
public static func getInstance()
```

#### `class PackedEnumAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func toString(val: Any)
public static func getInstance()
```

#### `class RepeatSInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedSInt32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatSInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedSInt64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatBoolAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedBoolAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatSFixed32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedSFixed32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatFixed32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedFixed32Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatFloatAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedFloatAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatSFixed64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedSFixed64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatFixed64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedFixed64Accessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatDoubleAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class PackedDoubleAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public static func getInstance()
```

#### `class RepeatStringAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func toString(val: Any)
public static func getInstance()
```

#### `class RepeatBytesAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func clone(val: Any)
public static func getInstance()
```

#### `class RepeatMessageAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func clone(val: Any)
public static func getInstance()
```

#### `class MapAccessor`

```
public func pack(tag: Tag, val: Any, out: Bytes)
public func unpack(tag: Tag, src: SimpleReader): Any
public func clone(val: Any)
public static func getInstance()
public func toString(val: Any)
```

#### `class MessageUnknownField`

```
MessageUnknownField()
public func toString()
```


#### `class SimpleReader`

```
SimpleReader()
public func size()
public func isEmpty()
public func seek(offset: Int64, origin!: SeekOrigin = CurPos)
public func peek(i: Int64)
public func data()
public func read()
public func read(s: Int64)
public func read(v: UInt8)
public func read(a: Collection<UInt8>)
```

#### `public func cj_generator(inbuf: ArrayList<UInt8>, outbuf: ArrayList<UInt8>): Unit`

从 stdin 读取二进制数据解析为 CodeGeneratorRequest, 根据该请求组织生成仓颉代码, 写入 CodeGeneratorResponse, 打包并输出到 stdout