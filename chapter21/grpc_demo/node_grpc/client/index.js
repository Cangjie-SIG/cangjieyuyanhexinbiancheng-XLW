var parseArgs = require('minimist');
var messages = require('../gen/hello_pb');
var services = require('../gen/hello_grpc_pb');

var grpc = require('@grpc/grpc-js');

function main() {
    var argv = parseArgs(process.argv.slice(2), {
        string: 'target'
    });
    var target;
    if (argv.target) {
        target = argv.target;
    } else {
        target = 'localhost:5001';
    }
    var client = new services.HelloClient(target,
        grpc.credentials.createInsecure());
    var request = new messages.HelloRequest();
    var user;
    if (argv._.length > 0) {
        user = argv._[0];
    } else {
        user = 'world';
    }
    request.setName(user);
    console.log(request)
    client.sayHello(request, function (err, response) {
        console.log('Greeting:', response.getMessage());
    });
}

main();