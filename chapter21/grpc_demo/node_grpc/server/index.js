const grpc = require("@grpc/grpc-js");
const services = require("../gen/hello_grpc_pb");
const messages = require("../gen/hello_pb");

/**
 * 实现SayHello RPC方法
 */
 function sayHello(call, callback) {
    var res = new messages.HelloResponse();
    console.log(call)
    res.setMessage('Hello '+call.request.getName());
    callback(null, res);
  }
  
  /**
   *启动接收Hello服务请求的RPC服务器
   *在示例服务器端口
   */
  function main() {
    var server = new grpc.Server();
    server.addService(services.HelloService, {sayHello: sayHello});
    server.bindAsync('0.0.0.0:8082', grpc.ServerCredentials.createInsecure(), () => {
      server.start();
    });
  }
  
  main();