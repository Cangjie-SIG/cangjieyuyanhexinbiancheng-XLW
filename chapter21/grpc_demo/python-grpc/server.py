#!/usr/bin/env python
# -*-coding: utf-8 -*-

from concurrent import futures
import grpc
import logging
import time

from rpc_package.hello_pb2_grpc import add_HelloServicer_to_server, HelloServicer
from rpc_package.hello_pb2 import HelloResponse


_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class Hello(HelloServicer):
    # 工作函数
    def sayHello(self, request, context):
        print(request.name)
        message = "此消息来自服务器，你好： \" " + request.name + " \""
        return HelloResponse(message=message)


def serve():
    # gRPC 服务器
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_HelloServicer_to_server(Hello(), server)
    server.add_insecure_port('[::]:8080')
    print("服务器正在打开，等待消息...")
    server.start()  # start()不会阻塞，如果运行时你的代码没有其它的事情可做，你可能需要循环等待。
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
