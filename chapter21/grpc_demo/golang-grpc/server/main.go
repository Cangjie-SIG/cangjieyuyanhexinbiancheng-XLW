package main

import (
	"grpc-demo/services"
	"log"
	"net"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const port = ":5001"

func main() {
	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalln(err.Error())
	}
	server := grpc.NewServer()
	// 注册 grpcurl 所需的 reflection 服务
	reflection.Register(server)
	// 注册业务服务
	services.RegisterHelloServer(server, &helloService{})

	log.Println("grpc server start ...")
	if err := server.Serve(listen); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}

type helloService struct{}

func (*helloService) SayHello(cxt context.Context, in *services.HelloRequest) (*services.HelloResponse, error) {
	return &services.HelloResponse{Message: "Hello " + in.GetName()}, nil
}
