## 仓颉单元测试包

### 介绍

src目录下的Connection.cj、ControlFlow.cj、HPACK.cj、HTTP2.cj是http2包的源码。

src目录下以`_test.cj`结尾的文件是测试上述源码的UT代码。

```shell
.
├── README.md
└── src
    ├── Connection.cj
    ├── Connection_test.cj
    ├── ControlFlow.cj
    ├── ControlFlow_test.cj
    ├── HPACK.cj
    ├── HPACK_test.cj
    ├── HTTP2.cj
    └── HTTP2_test.cj
```

### 编译

```shell
cd ./UintTest
cjc ./src/* --test
```

要编译UT代码需要加上`--test`。

### 运行
生成的可执行程序默认为根目录下的`main`。
```shell
root@387ae0d1cdb5:~/code/project/ut# ./main
-1
-1
-1
-1
-1
-1
-1
-1
-1
-1
start http2 server
start http2 client
--------------------------------------------------------------------------------------------------
TP: http2, time elapsed: 0 millis, Result:
    TCS: TestConnection, time elapsed: 0 millis, RESULT:
    [ PASS  ] CASE: testStartConn
    [ PASS  ] CASE: testSetConn
    [ PASS  ] CASE: testGetConn
    TCS: TestControlFlow, time elapsed: 0 millis, RESULT:
    [ PASS  ] CASE: testSetVal
    [ FAIL  ] CASE: testGetVal
    |ASSERTION|LEFT EXPRESSION|LEFT VALUE| RIGHT EXPRESSION  |RIGHT VALUE| MESSAGE  |
    | EXPECT  |   {ddddd}     | {ddddd}  |ctl . getVal ( 2 ) |$$$$dddddd |          |
    TCS: TestHpack, time elapsed: 0 millis, RESULT:
    [ PASS  ] CASE: testSetPackVal
    [ PASS  ] CASE: testGetPackVal
    |ASSERTION|LEFT EXPRESSION|LEFT VALUE|RIGHT EXPRESSION|RIGHT VALUE| MESSAGE  |
    | EXPECT  |     val       |  668899  |    668899      |  668899   |          |
    TCS: TestHttp2, time elapsed: 0 millis, RESULT:
    [ PASS  ] CASE: testServer
    [ PASS  ] CASE: testClient
Summary: 
    TOTAL: 9, SKIPPED: 0, PASS: 8, FAILED: 1, ERROR: 0
--------------------------------------------------------------------------------------------------
```
