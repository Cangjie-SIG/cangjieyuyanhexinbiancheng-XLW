type Int = Int64

func main() {
    let list = LinkedList<Int>()
    list.append(3)
    list.prepend(1)
    list.insert(1, 2)
    println(list.getFirst())
    println(list.get(1))
    println(list.getLast())
    println("------")
    list.removeFirst()
    list.removeLast()
    list.prepend(5)
    list.append(7)
    list.set(1, 6)
    for (v in list) {
        println(v)
    }
}

class Node<T> {
    Node(var prev!: ?Node<T>, var value!: T, var next!: ?Node<T>) {}
}

class LinkedList<T> <: Collection<T> {
    private var mySize: Int = 0
    private var first: ?Node<T> = None
    private var last: ?Node<T> = None

    public func size(): Int {
        mySize
    }

    public func isEmpty(): Bool {
        mySize == 0
    }

    public func iterator(): Iterator<T> {
        LinkedListIterator(first)
    }

    public func getFirst(): T {
        match (first) {
            case Some(v) => v.value
            case _ => throw IndexOutOfBoundsException()
        }
    }

    public func getLast(): T {
        match (last) {
            case Some(v) => v.value
            case _ => throw IndexOutOfBoundsException()
        }
    }

    public func get(index: Int): T {
        if (index < 0 || index >= mySize) {
            throw IndexOutOfBoundsException()
        }
        match (at(index)) {
            case Some(v) => v.value
            case _ => throw IndexOutOfBoundsException()
        }
    }

    public func set(index: Int, element: T): T {
        if (index < 0 || index >= mySize) {
            throw IndexOutOfBoundsException()
        }
        match (at(index)) {
            case Some(v) =>
                let oldValue = v.value
                v.value = element
                oldValue
            case _ => throw IndexOutOfBoundsException()
        }
    }

    public func prepend(element: T): Unit {
        linkFirst(element)
    }

    public func append(element: T): Unit {
        linkLast(element)
    }

    public func insert(index: Int, element: T): Unit {
        if (index < 0 || index > mySize) {
            throw IndexOutOfBoundsException()
        }
        if (index == 0) {
            linkLast(element)
        } else {
            linkBefore(element, at(index))
        }
    }

    public func remove(index: Int): T {
        if (index < 0 || index >= mySize) {
            throw IndexOutOfBoundsException()
        }
        match (at(index)) {
            case Some(v) => unlink(v)
            case _ => throw IndexOutOfBoundsException()
        }
    }

    public func removeFirst(): T {
        match (first) {
            case Some(v) => unlinkFirst(v)
            case _ => throw IndexOutOfBoundsException()
        }
    }

    public func removeLast(): T {
        match (last) {
            case Some(v) => unlinkLast(v)
            case _ => throw IndexOutOfBoundsException()
        }
    }

    private func at(index: Int): ?Node<T> {
        if (index < (mySize >> 1)) {
            var x = first
            for (i in 0..index) {
                match (x) {
                    case Some(v) => x = v.next
                    case _ => ()
                }
            }
            x
        } else {
            var x = last
            var i = mySize - 1
            while (i > index) {
                match (x) {
                    case Some(v) => x = v.prev
                    case _ => ()
                }
                i--
            }
            x
        }
    }

    private func linkFirst(element: T): Unit {
        let first = this.first
        let newNode = Node(prev: None<Node<T>>, value: element, next: first)
        this.first = newNode
        match (first) {
            case Some(v) => v.prev = newNode
            case _ => last = newNode
        }
        mySize++
    }

    private func linkLast(element: T): Unit {
        let last = this.last
        let newNode = Node(prev: last, value: element, next: None<Node<T>>)
        this.last = newNode
        match (last) {
            case Some(v) => v.next = newNode
            case _ => first = newNode
        }
        mySize++
    }

    private func linkBefore(element: T, succ: ?Node<T>): Unit {
        match (succ) {
            case Some(v) =>
                let pred = v.prev
                let newNode = Node(prev: pred, value: element, next: succ)
                v.prev = newNode
                match (pred) {
                    case Some(v2) => v2.next = newNode
                    case _ => first = newNode
                }
                mySize++
            case _ => ()
        }
    }

    private func unlinkFirst(x: Node<T>): T {
        let element = x.value
        let next = x.next
        x.next = None
        this.first = next
        match (next) {
            case Some(v) => v.prev = None
            case _ => last = None
        }
        mySize--
        element
    }

    private func unlinkLast(x: Node<T>): T {
        let element = x.value
        let prev = x.prev
        x.prev = None
        this.last = prev
        match (prev) {
            case Some(v) => v.next = None
            case _ => first = None
        }
        mySize--
        element
    }

    private func unlink(x: Node<T>): T {
        let element = x.value
        let next = x.next
        let prev = x.prev
        match (prev) {
            case Some(v) =>
                v.next = next
                x.prev = None
            case _ =>
                first = next
        }
        match (next) {
            case Some(v) =>
                v.prev = prev
                x.next = None
            case _ =>
                last = prev
        }
        mySize--
        element
    }
}

class LinkedListIterator<T> <: Iterator<T> {
    LinkedListIterator(var current: ?Node<T>) {}

    public func next(): ?T {
        match (current) {
            case Some(v) =>
                let element = v.value
                current = v.next
                element
            case _ => None
        }
    }

    public func iterator(): Iterator<T> {
        this
    }
}
