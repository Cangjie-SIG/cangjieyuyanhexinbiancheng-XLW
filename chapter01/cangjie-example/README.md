# CangjieExample

#### 介绍

仓颉代码示例，直接看代码吧！
#### 安装教程

1.  当前系统只在ubuntu.18.04版本环境下。
2.  在ubuntu18.04环境里输入如下命令：
    $ lsb_release -a 
    Distributor: ID: Ubuntu
    Description: Ubuntu 18.04.5 LTS 
    Release:     18.04 
    Codename:    bionic
3.  安装仓颉工具链：
$ apt-get install \ binutils \ libgcc-7-dev

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
