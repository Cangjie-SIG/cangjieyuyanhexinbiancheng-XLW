/*
 * Copyright (c) Cangjie Library Team 2022-2022. All rights resvered.
 */

#include<stdio.h>
#define MAX_LEN 1024
int g_EnvData[MAX_LEN] = {0};
const char *g_LibName[MAX_LEN] = {"OpenHarmony", "Skia", "Flutter", "Http2",
                                   "websocket", "gRPC", "glide", "greenDAO",
                                   "eventbus", "lottie", "rabbitmq", "deepface",
                                   "pytorch", "ffmpeg", "kafka", "ZKCLient"};
int cjNativePrintHello(void)
{
    printf("Hello cangjie! It is invoking c native api.\r\n");
    return 0;
}

int cjNativePrintSomething(const char *input)
{
    printf("%s\r\n", input);
    return 0;
}

int cjNativeSetVal(int index, int val)
{
    if (index >= MAX_LEN || index < 0) {
        printf("index(%d) is out of range\r\n", index);
        return -1;
    }
    g_EnvData[index] = val;
    printf("g_EnvData[%d] = %d, done!\r\n", index, val);
    return 0;
}

const char* cjNativeGetLibName(int index)
{
    if (index >= MAX_LEN || index < 0) {
        printf("index(%d) is out of range\r\n", index);
        return NULL;
    }
    const char* val = g_LibName[index];
    printf("g_LibName[%d] = %s, done!\r\n", index, val);
    return val;
}



