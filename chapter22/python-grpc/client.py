#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import logging
import grpc
from rpc_package import hello_pb2_grpc
from rpc_package import hello_pb2
from rpc_package.hello_pb2 import HelloRequest, HelloResponse
from rpc_package.hello_pb2_grpc import HelloStub


def run():
    # 使用with语法保证channel自动close
    with grpc.insecure_channel('localhost:8080') as channel:
        # 客户端通过stub来实现rpc通信
        stub = hello_pb2_grpc.HelloStub(channel)
        # 客户端必须使用定义好的类型，这里是HelloRequest类型
        response = stub.sayHello(hello_pb2.HelloRequest(name='cangjie'))
    print("Hello客户端接收到: " + response.message)

if __name__ == '__main__':
    logging.basicConfig()
    run()